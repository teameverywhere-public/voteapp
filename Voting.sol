pragma solidity >=0.4.0 <0.6.0;

contract Voting {
  
  mapping (bytes32 => uint256) public voted;
  bytes32[] public petList;

  constructor(bytes32[] memory pets) public {
    petList = pets;
  }
  function getVoted(bytes32 pet) view public returns (uint256) {
    return voted[pet];
  }
  function vote(bytes32 pet) public {
    voted[pet] += 1;
  }

}