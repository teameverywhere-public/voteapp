var cat = document.getElementById('cat');
var gozila = document.getElementById('gozila');
var dog = document.getElementById('dog');

cat.addEventListener("click", function(){vote("cat")}, false);
gozila.addEventListener("click", function(){vote("gozila")}, false);
dog.addEventListener("click", function(){vote("dog")}, false);


//web3 객채 셍성
web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"))

// if (typeof web3 !== 'undefined') {
//   web3 = new Web3(web3.currentProvider);
//  } else {
//   // set the provider you want from Web3.providers
//   web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
//  }
var account;

//api update
// EVM에 솔리디티 컨트랙트 호출을 할 때 인코딩을 하거나, 트랜잭션들로부터 데이터를 읽는 인터페이스
abi = JSON.parse('[{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"petList","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"bytes32"}],"name":"voted","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"pet","type":"bytes32"}],"name":"vote","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"pet","type":"bytes32"}],"name":"getVoted","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"pets","type":"bytes32[]"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"}]')

//creat contract Object
VotingContract = web3.eth.contract(abi);

//node console로 deploy한 주소 확인
contractInstance = VotingContract.at('0xa79d70256680977bd41acf4376abeb5272e3e548');



function vote(obj) {
  contractInstance.vote(obj, {from: web3.eth.accounts[0], gas: 4700000}, function() {
  // contractInstance.vote(obj, {from: web3.eth.accounts[0], gas: web3.eth.defaultGasPrice}, function() {
    console.log("vote called : "+obj)
    // console.log("web3.eth.defaultGasPrice  : "+web3.eth.defaultGasPrice.toString())
      $("#label" + obj).html(contractInstance.getVoted.call(obj).toString());
     });
 
}


$(document).ready(function() {

   web3.eth.getAccounts(function (err, accs) {
   if (err != null) {
    alert('There was an error fetching your accounts.')
    return
   }

   if (accs.length === 0) {
    alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.")
    return
   }

   account = accs[0]
   console.log("account : ", account)
  })

});



