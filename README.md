

installl nodejs
npm install -g ganache-cli

//사전 실행
node_modules/.bin/ganache-cli

node console compile
In the node console
> code = fs.readFileSync('Voting.sol').toString()
> solc = require('solc')
> compiledCode = solc.compile(code) 
 //이더리움 가상 머신(EVM)에서 동작하기 위해 바이트 코드로 변환된 컨트랙트
> Web3 = require('web3')
> web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
> compiledCode.contracts[':Voting'].interface
> abiDefinition = JSON.parse(compiledCode.contracts[':Voting'].interface)  
  //Application Binary Interface : 컨트랙트의 함수와 매개변수들을 JSON 형식으로 나타낸 리스트다. 
    스마트 컨트랙트의 함수를 이용하기 위해서는 ABI를 사용하여 함수를 해시할 수 있어야 한다
> VotingContract = web3.eth.contract(abiDefinition)
  // 컨트랙트 콜을 lower level 콜로 변환해주는 web3 API
> byteCode = compiledCode.contracts[':Voting'].bytecode
> deployedContract = VotingContract.new(['dog','cat','gozila'],{data: byteCode, from: web3.eth.accounts[0], gas: 4700000})

> deployedContract.address
> deployedContract.getVoted.call('dog')
> deployedContract.vote('dog', {from: web3.eth.accounts[0]})
> deployedContract.vote('dog', {from: web3.eth.accounts[0]})
> deployedContract.vote('dog', {from: web3.eth.accounts[0]})
> deployedContract.getVoted.call('dog').toLocaleString()

//run 
npm install -g http-server
http-server
Starting up http-server, serving ./
Available on:
http://127.0.0.1:8080